// alert("hello again!");
/* kjhkjhkj
lkhlkhlkhlk*/
//Syntax is the set of rules that describes how statements must be contstucted

// alert("good job")

//this is a statement with a correct syntax
console.log("Hello world 4");
//JS is loose type programming language
console.  log (  "HELLO WORLD!"  );

//and also with this
console.
log
(
"whaaaat!"
)

console.
log
(
"adfggwhaaaat!"
)

let myVariable;
let hello;

console.log(hello);



/* guidelines for declaring a variable
1. use let keyword followed by a variable name and then followed by = sign
2.variable names should start with lowercase letter
3. for constant variable we are using const keyword
4. variable names should be comprehenive or descriptive
5. Do not use spaces in declaring variables
6. all variable names should be unique
*/

//DIFFERENT CASING STYLES
// camel case - thisIsCamelCasing
// snake case - this_is_snake_casing
//kebab case - this-is-kebab-casing

let firstName = "eric";
let n = 1;

console.log(firstName);




//Declaring and Initializing variables
//Syntax -> let/const varialbeName = value;

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productName + " P" + productPrice);

//re-assigning a value to a variable

productPrice=25000;
console.log(productName + " P" + productPrice);

let friend = "Kate";

console.log(friend);

let supplier;
supplier="John Smith Tradings";
console.log(supplier);

supplier="Zuitt Store";
console.log(supplier);

//let vs const

const a=1;
console.log(a);

// local/global scope variable

let outerVariable = "hi"; //this is a global variable
{
	let innerVariable = "Hello Again!";  //this is a local variable
	console.log(innerVariable)
	console.log(outerVariable);
}

console.log(outerVariable);
// console.log(innerVariable); this will throw error 
//inner variables are in curly braces


//MULTIPLE VARIABLE DECLARATION
//Multiple Variable may be declared in one line
//convenient and easier to read

// let productCode = "DC017";
// let productBrand = "Dell";

let productCode = "DC017", productBrand="Dell", product1Price=300;
console.log(product1Price);

//let is a reserved keyword

// [SECTION]	DATA TYPES

//Strings - series of characters

let addNo = 12
let addNo2 = 12
let addSt ="J miranda";

console.log(addNo+addNo2+addSt);

//escape characters (\)
// "\n" - new line


let mailAddress = "Metro Manila\n\nPhilippines";

console.log(mailAddress);

let message = "John's employees went home ealy";

console.log(message);

//NUMBERS
//INtegers or whole numbers

let headcount = 26;
let heads = "52";

console.log(headcount+heads); //2652

//Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance); //20000000000

//Boolean True or False

//Arrays
//Are special kind of data type
//used to store multiple values with the same data type(not really)
//Syntax -> let/const arrayName = [elementA, elementB,...]

let grades = [98.7,92.1,90.2];
console.log(grades);

let details = ["john", "Smith", 32, true];
console.log(details);


//OBJECTS DATA TYPES
//uses curly braces
//Holds properties that describe the variable
//Syntax -> let/const objectName = {propertyA: value, propertyB: value}

let person = {

	fullName: "Juan Dela Cruz", 
	age: 35,
	contact: ["09985759223", "9958445512"],
	address: {
		houseNo: "345",
		city: "Manila",
	}

}

console.log(person);

let myGrades ={
	firstGrading:98.7,
	secondGrading:92.1,
	thirdGrading:90.2,
	fourthGrading:94.6
}
console.log(myGrades);

//checking of data type
console.log(typeof myGrades);


//in programming we always start on 0
//arrays, however in const, can be changed its values
const anime = ["one piece", "one punch man","attack on titan"];
//anime=["kimetsu no yaiba"];
anime[0] = "kimetsu no yaiba";
console.log(anime);

//null vs undefined
//null-when variable has emply value
//undefined-when a variable has no declared value
let spouse=null;







